public class MergeSortedArray {

    static void mergeNum(int[] num1,int[] num2){
        for(int i=0; i<num2.length; i++){
            for(int j=0; j<num1.length; j++){
                if(num1[j]==0){
                    num1[j] = num2[i];
                    break;
                }else{
                    if(num1[j]>=num2[i]){
                        for(int k=num1.length-1; k>j; k--){
                            num1[k] = num1[k-1];
                        }
                        num1[j] = num2[i];
                        break;
                    }
                }
            }
        }
    }

    static void sortNum(int[] arr){
        for(int i=0; i<numberOf(arr)-1; i++){
            if(arr[i]>=arr[i+1]){
                int temp = arr[i];
                arr[i] = arr[i+1];
                arr[i+1] = temp;
            }
        }
    }

    static void printArray(int[] arr){
        System.out.print("[");
        for(int i=0; i<arr.length; i++){
            if(i==arr.length-1){
                System.out.print(arr[i]);
            }else{
                System.out.print(arr[i]+",");
            }
        }
        System.out.println("]");
    }

    static int numberOf(int[] arr){
        int number = 0;
        for(int i=0; i<arr.length; i++){
            if(arr[i]!=0){        
                number++;
            }
        }
        return number;
    }

    public static void main(String[] args) {
        int[] num1 = {1,2,3,0,0,0};
        int[] num2 = {2,5,6};

        System.out.print("nums1 = ");
        printArray(num1);
        System.out.println("m = "+numberOf(num1));

        System.out.print("nums2 = ");
        printArray(num2);
        System.out.println("n = "+numberOf(num2));

        sortNum(num1);
        mergeNum(num1, num2);
        System.out.print("The result of the merge is ");
        printArray(num1);
    }
}
